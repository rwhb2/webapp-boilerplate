import os
import re
import sys
import sh

import pytest
from binaryornot.check import is_binary

PATTERN = "{{( ?cookiecutter)[.](.*?)}}"  # noqa: W605
RE_OBJ = re.compile(PATTERN)


@pytest.fixture
def context():
    return {
        "project_name": "Test Project",
        "project_slug": "test_project",
        "application_name": "Test Application",
        "api_slug": "testapi",
        "ui_slug": "testui",
    }


def build_files_list(root_dir):
    """Build a list containing absolute paths to the generated files."""
    return [
        os.path.join(dirpath, file_path)
        for dirpath, subdirs, files in os.walk(root_dir)
        for file_path in files
    ]


def check_paths(paths):
    """Method to check all paths have correct substitutions,
    used by other tests cases
    """
    # Assert that no match is found in any of the files
    for path in paths:
        if is_binary(path):
            continue

        for line in open(path, "r"):
            match = RE_OBJ.search(line)
            msg = "cookiecutter variable not replaced in {}"
            assert match is None, msg.format(path)


def test_default_configuration(cookies, context):
    """generated project should have no cookiecutter variables left"""
    result = cookies.bake(extra_context=context)
    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.basename == context["project_slug"]
    assert result.project.isdir()

    paths = build_files_list(str(result.project))
    assert paths
    check_paths(paths)


def test_tox(capsys, cookies, context):
    """generated project should pass the tox tests"""
    result = cookies.bake(context)

    # The output from tox is of interest when running the test suite.
    with capsys.disabled():
        # build frontend
        frontend_dir = os.path.join(result.project, context['ui_slug'], 'frontend')
        sh.apk('add', 'npm>=6', _err_to_out=True, _out=sys.stdout)
        sh.npm('install', _cwd=str(frontend_dir), _err_to_out=True, _out=sys.stdout)
        sh.npm('run', 'build', _cwd=str(frontend_dir), _err_to_out=True, _out=sys.stdout)
        os.environ['DJANGO_FRONTEND_APP_BUILD_DIR'] = str(os.path.join(frontend_dir, 'build'))
        os.environ['EXTERNAL_SETTING_SECRET_KEY'] = 'dummy-secret-key'
        os.environ['EXTERNAL_SETTING_SOCIAL_AUTH_GOOGLE_OAUTH2_KEY'] = 'placeholder-key'
        os.environ['EXTERNAL_SETTING_SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET'] = 'placeholder-secret'

        # Really we should be using Postgres but it is extremely fiddly to
        # set up and the boilerplate application doesn't define any database
        # models so nothing database-related is actually tested.
        os.environ['EXTERNAL_SETTING_DATABASES'] = '''{
            "default": {
                "ENGINE":"django.db.backends.sqlite3",
                "NAME": "/tmp/testing.sqlite",
            },
        }'''

        # run tox
        sh.tox(_cwd=str(result.project), _err_to_out=True, _out=sys.stdout)
