"""
A view which exposes the available versions of the API.

"""

from django.urls.base import reverse
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .versions import AVAILABLE_VERSIONS, VERSIONS_TO_APP_NAME


@method_decorator(name='get', decorator=swagger_auto_schema(
    tags=('Versions',),
    operation_summary='List API Versions',
    operation_description='Lists the available versions of the API',
    responses={
        200: openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                version_name: {
                    'type': openapi.TYPE_STRING,
                    'format': openapi.FORMAT_URI,
                    'example': f'<url for version {version_name}>',
                    'description': f'The URI for version {version_name} of the API'
                }
                for version_name in AVAILABLE_VERSIONS
            },
            required=list(AVAILABLE_VERSIONS),
        )
    }
))
class VersionsView(APIView):
    # Disable versioning for this viewset as there will not be a 'version' param
    # in the url.
    versioning_class = None

    def get(self, request):
        return Response({
            version: request.build_absolute_uri(reverse(f'{app_name}:api-root', args=[version]))
            for version, app_name in VERSIONS_TO_APP_NAME.items()
        })
