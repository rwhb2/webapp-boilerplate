"""
URL routing schema for API

"""
from rest_framework import routers

app_name = 'v1alpha'

router = routers.DefaultRouter()

urlpatterns = router.urls
