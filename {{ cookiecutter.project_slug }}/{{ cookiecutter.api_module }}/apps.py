from django.apps import AppConfig


class Config(AppConfig):
    name = '{{ cookiecutter.api_module }}'
    verbose_name = '{{ cookiecutter.api_name }}'
