from django.apps import AppConfig


class Config(AppConfig):
    name = '{{ cookiecutter.ui_module }}'
    verbose_name = '{{ cookiecutter.ui_name }}'
