import os
import sys

import externalsettings

from {{ cookiecutter.api_module }}.versions import AVAILABLE_VERSIONS

# By default, make use of connection pooling for the default database and use the Postgres engine.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'CONN_MAX_AGE': 60,  # seconds
    },
}

# If the EXTRA_SETTINGS_URLS environment variable is set, it is a comma-separated list of URLs from
# which to fetch additional settings as YAML-formatted documents. The documents should be
# dictionaries and top-level keys are imported into this module's global values.
_external_setting_urls = []
_external_setting_urls_list = os.environ.get('EXTRA_SETTINGS_URLS', '').strip()
if _external_setting_urls_list != '':
    _external_setting_urls.extend(_external_setting_urls_list.split(','))

externalsettings.load_external_settings(
    globals(), urls=_external_setting_urls,
    required_settings=[
        'SECRET_KEY', 'DATABASES',
        'SOCIAL_AUTH_GOOGLE_OAUTH2_KEY', 'SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET',
    ],
    optional_settings=[
        'EMAIL_HOST', 'EMAIL_HOST_PASSWORD', 'EMAIL_HOST_USER', 'EMAIL_PORT',
    ],
)

#: Base directory containing the project. Build paths inside the project via
#: ``os.path.join(BASE_DIR, ...)``.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#: SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

#: By default, all hosts are allowed.
ALLOWED_HOSTS = ['*']

#: Installed applications
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'whitenoise.runserver_nostatic',  # use whitenoise even in development
    'django.contrib.staticfiles',

    'automationcommon',
    'crispy_forms',
    'django_filters',
    'drf_yasg',
    'rest_framework',
    'rest_framework.authtoken',
    'social_django',
{% if cookiecutter.use_ucamlookup == "YES" %}
    'ucamlookup',
{% endif %}

    '{{ cookiecutter.application_module }}',

    '{{ cookiecutter.ui_module }}',
    '{{ cookiecutter.api_module }}',
]

#: Installed middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

#: Login configuration
SOCIAL_AUTH_URL_NAMESPACE = 'social'
SOCIAL_AUTH_POSTGRES_JSONFIELD = True

SOCIAL_AUTH_GOOGLE_OAUTH2_AUTH_EXTRA_ARGUMENTS = {'hd': 'cam.ac.uk'}
SOCIAL_AUTH_GOOGLE_OAUTH2_WHITELISTED_DOMAINS = ['cam.ac.uk']
SOCIAL_AUTH_GOOGLE_OAUTH2_WHITELISTED_HOSTED_DOMAINS = ['cam.ac.uk']

SOCIAL_AUTH_PIPELINE = (
    'social_core.pipeline.social_auth.social_details',
    'social_core.pipeline.social_auth.social_uid',
    'social_core.pipeline.social_auth.auth_allowed',
    '{{ cookiecutter.project_module }}.pipelines.enforce_hosted_domain',
    'social_core.pipeline.social_auth.social_user',
    'social_core.pipeline.user.get_username',
    'social_core.pipeline.user.create_user',
    'social_core.pipeline.social_auth.associate_user',
    'social_core.pipeline.social_auth.load_extra_data',
    'social_core.pipeline.user.user_details',
)

LOGIN_URL = '/accounts/login/google-oauth2/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

#: Root URL patterns
ROOT_URLCONF = '{{ cookiecutter.project_module }}.urls'

# Serve the frontend files from the application root.
FRONTEND_APP_BUILD_DIR = os.environ.get(
    'DJANGO_FRONTEND_APP_BUILD_DIR',
    os.path.abspath(os.path.join(BASE_DIR, '{{ cookiecutter.ui_module }}', 'frontend', 'build'))
)

#: Template loading
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [FRONTEND_APP_BUILD_DIR],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
                'social_django.context_processors.login_redirect',
            ],
        },
    },
]

#: WSGI
WSGI_APPLICATION = '{{ cookiecutter.project_module }}.wsgi.application'


#: Password validation
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


#: Internationalization
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/topics/i18n/
LANGUAGE_CODE = 'en-gb'

#: Internationalization
TIME_ZONE = 'UTC'

#: Internationalization
USE_I18N = True

#: Internationalization
USE_L10N = True

#: Internationalization
USE_TZ = True

#: Static files (CSS, JavaScript, Images)
#:
#: .. seealso:: https://docs.djangoproject.com/en/2.0/howto/static-files/
STATIC_URL = '/static/'

#: Authentication backends
AUTHENTICATION_BACKENDS = [
    'social_core.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

# Configure DRF to use Django's session authentication to determine the current user
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        '{{ cookiecutter.api_module }}.authentication.BearerTokenAuthentication',
    ],
    'DEFAULT_RENDERER_CLASSES': [
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
        'djangorestframework_camel_case.parser.CamelCaseFormParser',
        'djangorestframework_camel_case.parser.CamelCaseMultiPartParser',
    ],
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',
    'ALLOWED_VERSIONS': AVAILABLE_VERSIONS,
}

# Allow all origins to access API.
CORS_URLS_REGEX = r'^.*$'
CORS_ORIGIN_ALLOW_ALL = True

STATIC_ROOT = os.environ.get('DJANGO_STATIC_ROOT', os.path.join(BASE_DIR, 'build', 'static'))

# If the build directory for the frontend actually exists, serve files for the root of the
# application from it. Print a warning otherwise.
if os.path.isdir(FRONTEND_APP_BUILD_DIR):
    WHITENOISE_ROOT = FRONTEND_APP_BUILD_DIR
else:
    print('Warning: FRONTEND_APP_BUILD_DIR does not exist. The frontend will not be served',
          file=sys.stderr)

# By default we a) redirect all HTTP traffic to HTTPS, b) set the HSTS header to a maximum age
# of 1 year (as per the consensus recommendation from a quick Google search) and c) advertise that
# we are willing to be "preloaded" into Chrome and Firefox's internal list of HTTPS-only sites.
# Set the DANGEROUS_DISABLE_HTTPS_REDIRECT variable to any non-blank value to disable this.
if os.environ.get('DANGEROUS_DISABLE_HTTPS_REDIRECT', '') == '':
    # Exempt the healtch-check endpoint from the HTTP->HTTPS redirect.
    SECURE_REDIRECT_EXEMPT = ['^healthy/?$']

    SECURE_SSL_REDIRECT = True
    SECURE_HSTS_SECONDS = 31536000  # == 1 year
    SECURE_HSTS_PRELOAD = True
else:
    print('Warning: HTTP to HTTPS redirect has been disabled.', file=sys.stderr)
{% if cookiecutter.respect_x_forward_header == "YES" %}
# We also support the X-Forwarded-Proto header to detect if we're behind a load balancer which does
# TLS termination for us. In future this setting might need to be moved to settings.docker or to be
# configured via an environment variable if we want to support a wider range of TLS terminating
# load balancers.
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
{% endif %}

# Whether to support the x_forwarded_host in constructing the canonical url.
USE_X_FORWARDED_HOST = {{ True if cookiecutter.respect_x_forward_header == "YES" else False }}

SWAGGER_SETTINGS = {
    # Describe token authentication in swagger definition
    'SECURITY_DEFINITIONS': {
        'Bearer': {
            'type': 'apiKey',
            'name': 'Authorization',
            'in': 'header',
        },
    },
}
