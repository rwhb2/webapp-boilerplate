"""{{ cookiecutter.project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

import automationcommon.views

# Django debug toolbar is only installed in developer builds
try:
    import debug_toolbar
    HAVE_DDT = True
except ImportError:
    HAVE_DDT = False

schema_view = get_schema_view(
   openapi.Info(
      title='{{ cookiecutter.project_name }}',
      default_version='v1alpha1',
      description='{{ cookiecutter.api_name }}',
      contact=openapi.Contact(email='devops@uis.cam.ac.uk'),
      license=openapi.License(name='MIT License'),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('status', automationcommon.views.status, name='status'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('social_django.urls', namespace='social')),
    # use `healthy` rather than `healthz` as Cloud Run reserves the use of the `/healthz` endpoint
    path('healthy', lambda request: HttpResponse('ok', content_type="text/plain"), name='healthy'),
    path('', include('{{ cookiecutter.ui_module }}.urls', namespace='{{ cookiecutter.ui_module }}')),

{% if cookiecutter.use_ucamlookup == "YES" %}
    # lookup/ibis urls
    path('ucamlookup/', include('ucamlookup.urls')),
{% endif %}

    # If creating an API only webapp then the following urls may need moving from
    # the 'api/' endpoint to the root.
    # You'll also need to update {{ cookiecutter.api_module }}/tests/test_versions_view.py

    # Include the base API urls - which gives a view of the available API versions
    path('api/', include('{{ cookiecutter.api_module }}.urls')),

    # Include the v1alpha urls - allowing for any release version, the version is validated
    # by setting REST_FRAMEWORK.ALLOWED_VERSIONS in settings.base
    re_path(
        r'^api/(?P<version>v1alpha\d{1,3})/',
        include('{{ cookiecutter.api_module }}.v1alpha.urls', 'v1alpha')),
    # API documentation
    re_path(
        r'^api/(?P<version>(.*))/swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=None), name='schema-json'),
]

# Selectively enable django debug toolbar URLs. Only if the toolbar is
# installed *and* DEBUG is True.
if HAVE_DDT and settings.DEBUG:
    urlpatterns = [
        path(r'__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
