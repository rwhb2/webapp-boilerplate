FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:3.2-py3.10-alpine

# Ensure packages are up to date and install some useful utilities
RUN apk update && apk add git vim postgresql-dev libffi-dev gcc musl-dev \
	libxml2-dev libxslt-dev

WORKDIR /usr/src/app
ADD . .
RUN pip install --no-cache-dir -r requirements.txt
